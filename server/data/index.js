var packages = require('./packages')

/**
 * Returns a copy of the package json
 * @returns Package[]
 */
function getPackages() {
    return Object.assign([], packages)
}

module.exports.getPackages = getPackages
