var path = require('path')
var express = require('express')
var _ = require('lodash')
var request = require('request-promise')

var sitePackage = require('../../package.json')
var engine = require('express-handlebars').engine
var data = require('../data')

const promise = require('bluebird/js/release/promise')

module.exports = (app) => {

    app.engine('handlebars', engine())
    app.set('view engine', 'handlebars')
    app.set('views', __dirname + '/views')
    app.use('/assets', express.static('server/web/assets'))

    app.get('/', (req, res) => {
        packages = _.sortBy(data.getPackages(), 'name');
        if(req.query.name) packages = _.filter(packages, package => package.name.toLowerCase().indexOf(req.query.name.toLowerCase()) > -1)
        res.render('landing-page', { packages: packages, name: req.query.name, siteVersion: sitePackage.version })
    })

    app.get('/package/:namespace/:package_slug', (req, res) => {
        let package = _.find(data.getPackages(), (p) => p.slug === req.params.package_slug && p.namespace === req.params.namespace);
        res.render('package-pick-version', { package: package, siteVersion: sitePackage.version })
    })

    /**
     * TODO! Clean up this dirty but beautiful mess
     */
    app.get('/package/raw/:namespace_slug/:package_slug/:release_slug', (req, res) => {
        request
            .get(`https://storage.googleapis.com/storage.infopack.io/${req.params.namespace_slug}/${req.params.package_slug}/${req.params.release_slug}/index.json`)
            .then(response => {
                let package = {}

                // override package version
                package.version = req.params.release_slug

                let jsonUrlData = JSON.parse(response)

                var ff;
                if(_.has(jsonUrlData, 'files')) {
                    // assume > 1.* of infopack manifest
                    ff = jsonUrlData.files
                    package.name = jsonUrlData.name
                    package.title = jsonUrlData.title
                    package.description = jsonUrlData.description
                    package.manifestVersion = "1"
                } else {
                    ff = jsonUrlData
                    package.manifestVersion = "0"
                }
                ff.files = _.map(ff, (f) => {
                    let comp = f.path
                    if(!_.has(f, 'extname')) f.extname = path.extname(f.path)
                    if(!_.has(f, 'dirname')) f.dirname = path.dirname(f.path)
                    if(!_.has(f, 'basename')) f.basename = path.basename(f.path)
                    // treat .partial.html as html
                    if(comp.substr(-13) === '.partial.html') comp = comp.replace('.partial.html', '.html')
                    f._groupKey = comp.replace(f.extname, '')
                    return f;
                })
                files = _.groupBy(ff.files, '_groupKey')
                files = _.map(files, (file, key) => {
                    return {
                        title: file[0].title,
                        description: file[0].description,
                        dirname: file[0].dirname,
                        files: _.map(file, (f) => {
                            return {
                                title: f.title,
                                path: f.path,
                                dirname: f.dirname,
                                basename: f.basename,
                                extname: f.extname,
                                metaHtmlPath: f.metaHtmlPath,
                                metaJsonPath: f.metaJsonPath
                            }
                        })
                    }
                })
                res.render('package-detail-raw', {
                    package: package,
                    files: files,
                    namespace: req.params.namespace_slug,
                    slug: req.params.package_slug,
                    siteVersion: sitePackage.version
                })
            })
    })

    app.get('/package/raw/:namespace_slug/:package_slug/:release_slug/*', (req, res) => {
        request
            .get(`https://storage.googleapis.com/storage.infopack.io/${req.params.namespace_slug}/${req.params.package_slug}/${req.params.release_slug}/index.json`)
            .then(response => {
                let package = {}
                let jsonUrlData = JSON.parse(response)

                var ff;
                if(_.has(jsonUrlData, 'files')) {
                    // assume > 1.* of infopack manifest
                    ff = jsonUrlData.files
                    package.name = jsonUrlData.name
                    package.title = jsonUrlData.title
                    package.description = jsonUrlData.description
                    package.manifestVersion = "1"
                } else {
                    ff = jsonUrlData
                    package.manifestVersion = "0"
                }
            })

        request
            .get(`https://storage.googleapis.com/storage.infopack.io/${req.params.namespace_slug}/${req.params.package_slug}/${req.params.release_slug}/index.json`)
            .then(package => JSON.parse(package))
            .then(package => {
                // override version
                package.version = req.params.release_slug
                if(!_.has(package, 'files')) {
                    // assume 0.* version of infopack manifest
                    return res.redirect('https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace_slug + '/' + req.params.package_slug + '/' + req.params.release_slug + '/assets/' + req.params.path)
                } else {
                    let file = _.find(package.files, (o) => o.path === req.params["0"])
                    return Promise.all([package, file, request.get('https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace_slug + '/' + req.params.package_slug + '/' + req.params.release_slug + '/' + file.path) ])
                        .then(([package, file, fileContent ]) => {
                            fileContent = fileContent || ""
                            res.render('path-detail', {
                                package: package,
                                file: file,
                                namespace: req.params.namespace_slug,
                                slug: req.params.package_slug,
                                path: req.params.path,
                                _isDiffable: ['.md', '.html'].indexOf(file.extname) > -1 ? true : false,
                                _diffUrl: `/diff?source=${encodeURIComponent(req.params.namespace_slug + ':' + req.params.package_slug)}:${package.version}:${encodeURIComponent(file.path)}`,
                                _isImg: ['.png', '.jpg', '.jpeg'].indexOf(file.extname) > -1 ? true : false,
                                _isHtml: ['.html'].indexOf(file.extname) > -1 ? true : false,
                                _isMd: ['.md'].indexOf(file.extname) > -1 ? true : false,
                                _isPdf: ['.pdf'].indexOf(file.extname) > -1 ? true : false,
                                fileContent: ['.html'].indexOf(file.extname) > -1 ? fileContent.replaceAll('src="./', 'src="https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace_slug + '/' + req.params.package_slug + '/' + req.params.release_slug + '/' + file.dirname + '/') : fileContent.replaceAll('](./', '](https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace_slug + '/' + req.params.package_slug + '/' + req.params.release_slug + '/' + file.dirname + '/'),
                                siteVersion: sitePackage.version
                            })
                        })
                }
                
            })
    })

    app.get('/package/:namespace/:package_slug/:version', (req, res) => {
        let package = _.find(data.getPackages(), (p) => p.slug === req.params.package_slug && p.namespace === req.params.namespace);
        if(!package) {
            return res.status(404).end()
        }
        let version = _.find(package.versions, (o) => o.version === req.params.version)
        if(!version) {
            return res.status(404).end()
        }
        request
            .get(version.jsonUrl)
            .then((jsonUrl) => {
                jsonUrlData = JSON.parse(jsonUrl)
                
                var ff;
                if(_.has(jsonUrlData, 'files')) {
                    // assume > 1.* of infopack manifest
                    ff = jsonUrlData.files
                    package.version = jsonUrlData.version
                    package.description = jsonUrlData.description
                    package.manifestVersion = "1"
                } else {
                    ff = jsonUrlData
                    package.version = version.version
                    package.manifestVersion = "0"
                }
                ff.files = _.map(ff, (f) => {
                    let comp = f.path
                    if(!_.has(f, 'extname')) f.extname = path.extname(f.path)
                    if(!_.has(f, 'dirname')) f.dirname = path.dirname(f.path)
                    if(!_.has(f, 'basename')) f.basename = path.basename(f.path)
                    // treat .partial.html as html
                    if(comp.substr(-13) === '.partial.html') comp = comp.replace('.partial.html', '.html')
                    f._groupKey = comp.replace(f.extname, '')
                    return f;
                })
                files = _.groupBy(ff.files, '_groupKey')
                files = _.map(files, (file, key) => {
                    return {
                        title: file[0].title,
                        description: file[0].description,
                        dirname: file[0].dirname,
                        files: _.map(file, (f) => {
                            return {
                                title: f.title,
                                path: f.path,
                                dirname: f.dirname,
                                basename: f.basename,
                                extname: f.extname,
                                metaHtmlPath: f.metaHtmlPath,
                                metaJsonPath: f.metaJsonPath
                            }
                        })
                    }
                })
                res.render('package-detail', {
                    package: package,
                    files: files,
                    namespace: req.params.namespace,
                    slug: req.params.package_slug,
                    siteVersion: sitePackage.version
                })
            })
    })

    app.get('/package/:namespace/:package_slug/:version/*', (req, res) => {
        let package = _.find(data.getPackages(), (p) => p.slug === req.params.package_slug && p.namespace === req.params.namespace);
        if(!package) {
            return res.status(404).end()
        }
        let version = _.find(package.versions, (o) => o.version === req.params.version)
        if(!version) {
            return res.status(404).end()
        }

        request
            .get(version.jsonUrl)
            .then(package => JSON.parse(package))
            .then(package => {
                if(!_.has(package, 'files')) {
                    // assume 0.* version of infopack manifest
                    return res.redirect('https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace + '/' + req.params.package_slug + '/' + req.params.version + '/assets/' + req.params.path)
                } else {
                    let file = _.find(package.files, (o) => o.path === req.params["0"])
                    return Promise.all([package, file, request.get('https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace + '/' + req.params.package_slug + '/' + req.params.version + '/' + file.path) ])
                        .then(([package, file, fileContent ]) => {
                            fileContent = fileContent || ""
                            res.render('path-detail', {
                                package: package,
                                file: file,
                                namespace: req.params.namespace,
                                slug: req.params.package_slug,
                                path: req.params.path,
                                _isDiffable: ['.md', '.html'].indexOf(file.extname) > -1 ? true : false,
                                _diffUrl: `/diff?source=${encodeURIComponent(req.params.namespace + ':' + req.params.package_slug)}:${package.version}:${encodeURIComponent(file.path)}`,
                                _isImg: ['.png', '.jpg', '.jpeg'].indexOf(file.extname) > -1 ? true : false,
                                _isHtml: ['.html'].indexOf(file.extname) > -1 ? true : false,
                                _isMd: ['.md'].indexOf(file.extname) > -1 ? true : false,
                                _isPdf: ['.pdf'].indexOf(file.extname) > -1 ? true : false,
                                fileContent: ['.html'].indexOf(file.extname) > -1 ? fileContent.replaceAll('src="./', 'src="https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace + '/' + req.params.package_slug + '/' + req.params.version + '/' + file.dirname + '/') : fileContent.replaceAll('](./', '](https://storage.googleapis.com/storage.infopack.io/' + req.params.namespace + '/' + req.params.package_slug + '/' + req.params.version + '/' + file.dirname + '/'),
                                siteVersion: sitePackage.version
                            })
                        })
                }
                
            })
    })

    app.get('/diff/', (req, res) => {
        res.render('diff', { query: req.query, siteVersion: sitePackage.version })
    })

    app.get('/collection/', (req, res) => {
        res.render('collection', {})
    })
}
