var express = require('express');

var app = express();

require('./api')(app);
require('./web')(app);

app.listen(9001 || process.env.PORT, () => {
    console.log('Listening...')
});

