var package = require('../../package')
var cors = require('cors')
var bodyParser = require('body-parser')
const express = require('express')

module.exports = (app) => {
    app.use(cors())
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(express.json())
    app.use('/api/package', require('./package'))
    app.use('/api/diff', require('./diff'))
    app.get('/api', (req, res) => res.json({ status: 'OK', server_version: package.version }))

}