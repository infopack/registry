var Diff = require('diff');
const { marked } = require('marked');
var request = require('request-promise');

function irnToUrl(irn) {
    const myRe = new RegExp("(.*):(.*):(.*):(.*)", "g")
    const result = myRe.exec(irn)
    return `${result[1]}/${result[2]}/${result[3]}/${result[4]}`
}

module.exports = function(source, target) {
    return Promise.all([
        request.get(`http://storage.infopack.io/${irnToUrl(source)}`),
        request.get(`http://storage.infopack.io/${irnToUrl(target)}`)
    ]).then(([oldString, newString]) => {
        const diff = Diff.diffWords(oldString, newString)
        let diffString = ''
        diff.forEach((part) => {
            if(part.added) diffString += `<span style="background-color: green; color: white;">${part.value}</span>`
            if(part.removed) diffString += `<span style="background-color: red; color: white;">${part.value}</span>`
            if(!part.added && !part.removed) diffString += part.value
        })
        return diffString
    }).then(str => {
        if(source.substr(-3) === '.md') {
            str = marked.parse(str)
        }
        return str
    })
}