var Router = require('express').Router
var _ = require('lodash')

var testDiff = require('./test-diff')

var router = new Router()

/**
 * 
 * @api {POST} /diff?name List
 * @apiName diff
 * @apiGroup Diff
 * @apiVersion  0.0.1
 * 
 * 
 * @apiParam  {String} [name=""] Filter packages by the name property
 * 
 * @apiSuccess (200) {Array} Package[] A list of packages
 * 
 */
router.post('/', (req, res) => {
    testDiff(
        req.body.source,
        req.body.target,
    ).then((diffString) => {
        res.send(diffString)
    }).catch((err) => {
        res.send("Kunde inte läsa filer. Har du angivit rätt addresser?");
    })
})

module.exports = router