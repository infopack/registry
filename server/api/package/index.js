var Router = require('express').Router
var _ = require('lodash')

var data = require('../../data')

var router = new Router()

/**
 * 
 * @api {GET} /package?name List
 * @apiName listPackages
 * @apiGroup Package
 * @apiVersion  0.0.1
 * 
 * 
 * @apiParam  {String} [name=""] Filter packages by the name property
 * 
 * @apiSuccess (200) {Array} Package[] A list of packages
 * 
 * 
 *
 * @apiSuccessExample {type} Success-Response:
 * [
 *     {
 *          "slug": "sbp-begreppslista",
 *          "namespace": "swe-nrb",
 *          "name": "Nationella riktlinjer - Begreppslista",
 *          "versions": [
 *              { "version": "latest", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/latest/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/latest/assets/output.json" },
 *              { "version": "1.0.0", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.0/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.0/assets/output.json" },
 *              { "version": "1.0.1", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.1/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.1/assets/output.json" },
 *              { "version": "1.1.0", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.0/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.0/assets/output.json" },
 *              { "version": "1.1.2", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.2/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.2/assets/output.json" },
 *              { "version": "1.1.3", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.3/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.3/assets/output.json" },
 *              { "version": "1.1.4", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.4/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.4/assets/output.json" },
 *              { "version": "1.1.5", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.5/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.5/assets/output.json" }
 *          ],
 *          "deprecated": true
 *      }
 * ]
 * 
 * 
 */
router.get('/', (req, res) => {
    let packages = data.getPackages();
    if(req.query.name) {
        packages = _.filter(packages, (package) => package.name.toLowerCase().indexOf(req.query.name.toLowerCase()) > -1)
    }
    res.json(_.sortBy(packages, 'name'))
})

/**
 * 
 * @api {GET} /package/:namespace/:package_slug Get infopack metainfo
 * @apiName getPackage
 * @apiGroup Package
 * @apiVersion  0.0.1
 * 
 * 
 * @apiParam  {String} namespace Infopack author namespace
 * @apiParam  {String} package_slug Infopack slug
 * 
 * @apiSuccess (200) {Array} Package[] A list of packages
 * 
 * 
 *
 * @apiSuccessExample {type} Success-Response:
 * {
 *     "slug": "sbp-begreppslista",
 *     "namespace": "swe-nrb",
 *     "name": "Nationella riktlinjer - Begreppslista",
 *     "versions": [
 *         { "version": "latest", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/latest/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/latest/assets/output.json" },
 *         { "version": "1.0.0", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.0/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.0/assets/output.json" },
 *         { "version": "1.0.1", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.1/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.1/assets/output.json" },
 *         { "version": "1.1.0", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.0/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.0/assets/output.json" },
 *         { "version": "1.1.2", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.2/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.2/assets/output.json" },
 *         { "version": "1.1.3", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.3/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.3/assets/output.json" },
 *         { "version": "1.1.4", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.4/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.4/assets/output.json" },
 *         { "version": "1.1.5", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.5/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.1.5/assets/output.json" }
 *      ],
 *      "deprecated": true
 * }
 * 
 * 
 */
router.get('/:namespace/:package_slug', (req, res) => {
    let package = _.find(data.getPackages(), { namespace: req.params.namespace, slug: req.params.package_slug })
    res.status(package ? 200: 404).json(package)
})

/**
 * 
 * @api {GET} /package/:namespace/:package_slug/version List
 * @apiName listVersions
 * @apiGroup Package > Version
 * @apiVersion  0.0.1
 * 
 * 
 * 
 * @apiSuccess (200) {Array} Version[] A list of available versions for the package
 * 
 * 
 *
 * @apiSuccessExample {type} Success-Response:
 * [
 *     { "version": "latest", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/latest/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/latest/assets/output.json" },
 *     { "version": "1.0.0", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.0/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.0/assets/output.json" },
 *     { "version": "1.0.1", "htmlUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.1/index.html", "jsonUrl": "http://storage.infopack.io/swe-nrb/sbp-begreppslista/1.0.1/assets/output.json" }
 * ]
 * 
 * 
 */
router.get('/:namespace/:package_slug/version', (req, res) => {
    let { versions, ...package } = _.find(data.getPackages(), { namespace: req.params.namespace, slug: req.params.package_slug })
    res.status(package ? 200: 404).json(versions)
})

module.exports = router