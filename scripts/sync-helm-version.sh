#!/bin/bash

sed -i "s/^appVersion:.*/appVersion: $(cat package.json | jq '.version')/" ./helm/Chart.yaml
